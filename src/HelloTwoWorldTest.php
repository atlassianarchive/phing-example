<?php

    require_once "PHPUnit/Framework/TestCase.php";
    require_once "HelloTwoWorld.php";

    /**
    * Test class for HelloWorld
    *
    * @author Michiel Rook
    * @version $Id$
    * @package hello.world
    */
    class HelloTwoWorldTest extends PHPUnit_Framework_TestCase
    {
        public function testSayHello()
        {
            $hello = new HelloTwoWorld();
            $this->assertEquals("Hello Two World!", $hello->sayHello());
        }
    }

?>